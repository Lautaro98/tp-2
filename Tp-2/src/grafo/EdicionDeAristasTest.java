package grafo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EdicionDeAristasTest {

	@Before
	public void setUp() throws Exception {
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void verticeNegativoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(-1, 3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void verticeExcedidoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(5, 2);
	}
	@Test(expected = IllegalArgumentException.class)
	public void segundoExcedidoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void segundoNegativoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, -1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarLoopTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 2);
	}



	@Test
	public void arsitaExistenteTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		assertTrue(grafo.existeArista(2, 3));
	}

	
	@Test
	public void aristaOpuestaTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		assertTrue(grafo.existeArista(3, 2));
	}
	
	@Test
	public void aristaInexistenteTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		assertFalse(grafo.existeArista(1, 4));
	}
	
	@Test
	public void eliminarAristaExistenteTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.eliminarArista(2, 4);
		
		assertFalse(grafo.existeArista(2, 4));
	}
	
	@Test
	public void eliminarAristaInexistenteTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 4);
		
		assertFalse(grafo.existeArista(2, 4));
	}
	
	@Test
	public void eliminarAristaDosVecesTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.eliminarArista(2, 4);
		grafo.eliminarArista(2, 4);
		
		assertFalse(grafo.existeArista(2, 4));
	}
	
	@Test
	public void AgregarAristaDosVecesTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 3);
		
		assertTrue(grafo.existeArista(2, 3));
	}
	
	@Test
	public void pesoAristaTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		assertTrue(grafo.pesoArista(2, 3)<1);
		assertTrue(grafo.pesoArista(3, 2)<1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void pesoAristaInexistenteTest() {
		
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		assertTrue(grafo.pesoArista(1, 2) < 1);
	
	}
	
}
