package grafo;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Grafo {
	
	//Representamos el grafo por su matriz de adyacencia
	public boolean [][] A;
	
	//peso de las aristas
	public float [][] pesoAristas;
	
	// la cantidad de vertices esta representada desde el constructor
	public Grafo(int vertices) {
		A = new boolean[vertices][vertices];
		pesoAristas = new float [vertices][vertices];
		
	}
	//
	//Agregado de aristas
	public void agregarArista(int i, int j) {
		//Reglas para una excepcion: tiene que ser descriptivo, ... ver semana 2, parte 6.
		//se lo suele llamar CODIGO DEFENSIVO.
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		A[i][j] = true;
		A[j][i] = true;
		Random random = new Random();
		float peso = random.nextFloat();
		pesoAristas[i][j] = peso;
		pesoAristas[j][i] = peso;
	}
	
	//Borrar de aristas
	public void eliminarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = false;
		A[j][i] = false;
		
	}

	
	//Informa si existe la arista especificada
	public boolean existeArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i,j);
		
		return A[i][j];
	}
	
	//Cantidad de vertices
	public int tamanioVertices() {
		return A.length;
	}
	
	//Vecinos de un vertice
	public Set<Integer> vecinos(int i){
		
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j< this.tamanioVertices(); ++j) if(i != j) {
			if(this.existeArista(i, j)) {
				ret.add(j);
			}	
		}
		return ret;
	}
	
	public float pesoArista(int i, int j) {
		
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i,j);
		if(existeArista(i,j)==false)
			throw new IllegalArgumentException("La arista " + i + ", " + j + " no existe");
			
		
		return pesoAristas[i][j];
	}
	
	
	//Verifica que sea un vertice adecuado
	private void verificarVertice(int i) {
		if(i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);

		if(i >= A.length)
			throw new IllegalArgumentException("El vertice deb estar entre 0 y |v|-1 : " + i);
	}

	//Verifica que i y J sean vertices distintos
	private void verificarDistintos(int i, int j) {
		if(i == j)
			throw new IllegalArgumentException("No se perimiten loops: (" + i + ", " + j + ")");
	}
	
	

}
