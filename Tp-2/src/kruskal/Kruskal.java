package kruskal;

import java.util.HashSet;
import java.util.Set;

import grafo.Grafo;

public class Kruskal {
	
	
//	public static float aristaMinima(Grafo grafo) {
//		float min = 1;
//		int Vi = 0;
//		int Vj = 0;
//		for(int i = 0; i< grafo.tamanioVertices(); i++) {
//			for(int j = 0; j< grafo.tamanioVertices(); j++) if(i != j) {
//				if(grafo.existeArista(i, j) && grafo.pesoArista(i, j) < min) {
//					min = grafo.pesoArista(i, j);
//					Vi = i;
//					Vj = j;
//				}
//			}
//		}
//
//		grafo.eliminarArista(Vi, Vj);
//		return min;
//	}
	
	public static float aristaMinima(Grafo grafo, Set<Float> conj) {
		float min = 1;
		int Vi = 0;
		int Vj = 0;
		for(int i = 0; i< grafo.tamanioVertices(); i++) {
			for(int j = 0; j< grafo.tamanioVertices(); j++) if(i != j) {
				if(grafo.existeArista(i, j) && grafo.pesoArista(i, j) < min && conj.contains(grafo.pesoArista(i, j)) == false) {
					min = grafo.pesoArista(i, j);
					Vi = i;
					Vj = j;
				}
			}
		}
		conj.add(grafo.pesoArista(Vi, Vj));
		return min;
	}
	
	public static Set<Float> conBFS(Grafo grafo){
		
		Set<Float> ret = new HashSet<Float>();
		
		aristaMinima(grafo, ret);
		aristaMinima(grafo, ret);
		aristaMinima(grafo, ret);
		aristaMinima(grafo, ret);
		aristaMinima(grafo, ret);

		return ret;
	}
	
	
	
	public static void main(String[] args) {
		Grafo grafo = new Grafo(5);
		Set<Float> ret = new HashSet<Float>();
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 4);
		grafo.agregarArista(4, 0);
		System.out.println(grafo.pesoArista(0, 1));
		System.out.println(grafo.pesoArista(1, 2));
		System.out.println(grafo.pesoArista(3, 4));
		System.out.println(grafo.pesoArista(4, 0));
		
		System.out.println("");
		//grafo.eliminarArista(1, 2);
		
		System.out.println(aristaMinima(grafo,ret));
		System.out.println(aristaMinima(grafo,ret));
		System.out.println(aristaMinima(grafo,ret));
		System.out.println(aristaMinima(grafo,ret));
		//System.out.println("");
		//System.out.println(conBFS(grafo));
		
	}

}


